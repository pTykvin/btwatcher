#!/bin/bash

_root=$(dirname `realpath $0`)

function unlock() {
  killall i3lock && echo "*Ноут разблокирован.* Надеюсь ты где-то рядом 😉" > $_root/../../var/latest_status
  return $?
}

function main() {
  unlock
  [[ $? -ne 0 ]] && { sleep 3; unlock; }
}

main 
