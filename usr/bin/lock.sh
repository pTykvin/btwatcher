#!/bin/bash

_root=$(dirname `realpath $0`)

function err() {
  echo "Какой-то факап 😱! *Ноут не залочился!*" > $_root/../../var/latest_status
  exit 
}

function main() {
  export DISPLAY=:0
  set -e
  i3lock-fancy --text "Hall of Fame\n\n@evgenymarkov: 1\n@olegmokhov: 2" &
  echo "*Ноут заблокирован.* Можешь не париться 😎" > $_root/../../var/latest_status
  set +e
}

trap 1 2 3 9 15 err

main 
