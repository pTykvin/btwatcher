#!/bin/bash
export DISPLAY=:0
i3-nagbar -m "BTHandler is running" -t warning -f 'pango:Hack 20' &>/dev/null &
sleep 3
killall i3-nagbar &>/dev/null
