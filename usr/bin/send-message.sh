#!/bin/bash

text=$(cat<<EOF
$1
$2

⏱ `date +"%b %d %H:%M:%S"`
EOF
)

curl -k -s -G "https://api.telegram.org:8080/bot$BOT/editMessageText" \
  --data-urlencode "chat_id=$CHAT" \
  --data-urlencode "message_id=22" \
  --data-urlencode "parse_mode=Markdown" \
  --data-urlencode "text=$text"\
  --max-time 10 \
  --retry 5 \
  --retry-delay 0 \
  --retry-max-time 40 \
  --retry-connrefuse > /dev/null
