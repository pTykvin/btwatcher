#!/bin/bash
set -x
MAC=$1
result=`hcitool rssi $MAC | grep RSSI`
code=$?
[[ $code == 0 ]] && echo $result | awk '{ print $4 }' || exit $code
set +x
