#!/bin/bash
root=`dirname $0`

for f in `ls -1 $root/var/*.pid`
do
  kill `cat $f`
done
